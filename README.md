# JShell
Shell for Operating Systems

## Task 1 ([V1.0](https://github.com/jfaldanam/JShell/tree/version1))

The code enters a loop where it reads and analyzes command lines typed from the keyboard.
The first stagefocuses on the task of creating a process and making a new program run. You must add
the necessary code to “Shell Project.c” in order to:

1. Execute the commands in an independent process
2. Wait or not to wait the termination of the command depending on whether the
command is a foreground or background process
3. Print the information about the command-process, its pid, state, etc. to screen
4. continue the loop to process the next command


In this task the student should use the system calls fork, execvp, waitpid and
exit. The information to be printed on the screen are: pid, name, type and code of
termination or the signal of the termination reason. For example:

`Foreground pid: 5615, command: ls, Exited, info: 0`

`Foreground pid: 5616, command: vi, Suspended, info: 20`

For background commands should be reported their pid, name and indication that they
are executing on background simultaneously with the Shell. For example:

`Background job running... pid: 5622, command: sleep`

When a command is not found and obviously cannot run, the Shell should print an
appropriate error message. For example:
`Error, command not found: lss`
To compile the program use either:
```bash
gcc –c Shell_project.c //This command will generate Shell_project.o
gcc –c job_control.c //This command will generate job_control.o
gcc -o Shell Shell_project.o job_control.o
```
or
```
gcc Shell_project.c job_control.c –o Shell
```
To execute the program use:
`./Shell`

To exit use: CONTROL + D (^D).

## Task 2 ([V2.0](https://github.com/jfaldanam/JShell/tree/version2))

A Shell should distinguish internal commands from external commands. Internal
commands are implemented by the Shell itself and external commands are executable
files that the Shell brings typically from /bin or /usr/bin. Up to this moment your
Shell does not provide any internal command.
1. Now, we start implementing the cd internal command,which allows changing the
current directory. To implement this command use function "chdir" .

2. To execute an external command in the Shell, the command should belong to an
independent process group so that the terminal can be assigned to one unique
foreground task at a time. Therefore, the child processes of the shell are assigned
a group id (i.e., gid) that differs from the parent id using function
`new_process_group()` function, wich is a macro that returns `setpgid()`.

3. A foreground child process must be the unique owner of the terminal since the
parent process is blocked in waitpid. The terminal should be given back to the
Shell using function `set_terminal()`, a macro that returns `tcsetpgrp()`. If
the task is sent to the background, it should not be given the terminal, because the
Shell should continue reading the keyboard, i.e. become the foreground job.

4. To make the Shell work correctly by giving the terminal to foreground tasks, it is
necessary to ignore all signals related to the terminal. This can be done by including
function `ignore_terminal_signals()` at the beginning of the program. When a
new process is created, before execution, the signal handlers should be restored to
the default operation (within the child process) using function
`restore_terminal_signals()`.

5. The value returned by `waitpid` in status indicates whether the child process
has terminated or is blocked. To detect the suspension of a child, the option
`WUNTRACED` in function `waitpid` should be added. Make sure that the
suspended foreground processes are reported by the Shell.

## Task 3 ([V3.0](https://github.com/jfaldanam/JShell/tree/version3))

Background commands terminate in a zombie state indicated by <defunct> when
using the `ps` command. Up to now the Shell does not take care of them.

1. Please, install a handler for `SIGCHLD` to handle the termination / suspension of
background tasks asynchronously using the `signal()` function. To do so,
create a task list using the functions from the “job_control” module and
add each command launched on background to that list.

2. When the handler of signal `SIGCHLD` is activated, we must review each list
entry to see if any background process has terminated or is suspended. To check
whether a process has completed without blocking the Shell, use the option
WNOHANG in function `waitpid()`.

3. This exercise should avoid leaving background tasks in zombie state and make
the Shell handle them and inform about their termination / suspension. When a
task is terminated or suspended, the list of tasks should be updated adequately.

## Task 4 ([V4.0](https://github.com/jfaldanam/JShell/tree/version4))

The implementation should safely manage background tasks , i.e. launched with '&'.
If the job is sent to the background it should not be associated to the terminal, because
the Shell needs to continue reading the keyboard; the shell comes to the foreground.

Add a job control list using types and functions defined in module `job_control.c`.
This list should allow controlling multiple running background tasks as well as multiple
suspended tasks. The suspended tasks can come from foreground suspended tasks
`(ctrl.+ Z)` or background tasks that were suspended after trying to read from the terminal.
Signal `SIGSTOP` will also suspend any foreground or background task.
The code we are designing is reentrant, i.e., while the Shell is running, signal `SIGCHLD`
could arrive and thus the shell executes the handler.

1. To avoid consistency problems between child and parent when accessing the
job-list, signal `SIGCHLD` should be blocked in the Shell-code wherever the job
list is modified. Signal `SIGCHLD` can be blocked by functions
(`block_SIGCHLD`, `unblock_SIGCHLD`) available in `job_control.c` .

2. The shell should implement the following internal commands:
  + “jobs” prints a list of background and suspended tasks, or a message if there
is none. The job list should include both, as well the list of tasks explicitly
sent to the background (with '&'), as the suspended (stopped ) tasks. A
function that prints a list of tasks already exists in job_control.

  + “fg” operates on the task list. The command puts a suspended or
background task to run on the foreground. The argument of this
command is the identifier of the place where that task in located in the task
list (1, 2, ...). If the argument is not specified, it should be applied to the first
task of the list (the last entry to the list). It is important that the signals are
sent to the entire process group using `killpg()`, equivalent to function
kill with a negative pid; please, study the help online.

_Note: The operations to be performed on a background process,
once found in the list are: yielding the terminal to it (was
under parent control), change only the necessary things in its
job structure and send it a signal to continue, if it is
suspended, for example, waiting to get the terminal._

  + “bg” works on the task list adding a task that is suspended running on the
background. The argument of this command should be the identifier of the
place of that process in the list (1, 2, 3, ...).
