/**
UNIX Shell Project

Sistemas Operativos
Grados I. Informatica, Computadores & Software
Dept. Arquitectura de Computadores - UMA

Some code adapted from "Fundamentos de Sistemas Operativos", Silberschatz et al.

To compile and run the program:
   $ gcc Shell_project.c job_control.c -o Shell
   $ ./Shell
	(then type ^D to exit program)

**/

#include "job_control.h"   // remember to compile with module job_control.c
#include "internal_commands.h"
#include <stdio.h>
#include <string.h>


#define MAX_LINE 256 /* 256 chars per line, per command, should be enough. */
#define KGRN  "\x1B[32m" //Color code for printf (Green)
#define KNRM  "\x1B[0m"  //Color code for printf (Reset)
#define MAX_MESSAGE 2048

int signalWaiting;
int background;
int signalReceived;
int info;
job* taskList;
int pid_fork;


//Checks if a command is internal
int checkIfInternal(char* command, char* internalCommands) {
	int found = 0;
	int n = 0;
	char* temp = (char*)malloc(100*sizeof(char));
	for(int i = 0; internalCommands[i] != '\0'; i++) {
		if (internalCommands[i] == ',') {
			free(temp);
			temp = (char*)malloc(100*sizeof(char));
			n++;
		} else {
			int len = strlen(temp);
      temp[len] = internalCommands[i];
      //temp[len+1] = '\0';
			if (strcmp(command,temp) == 0) {
				found = n;
			}
		}
	}
	return found;
}

/* reverse:  reverse string s in place */
void reverse(char s[]) {
    int i, j;
    char c;

    for (i = 0, j = strlen(s) - 1; i < j; i++, j--) {
        c = s[i];
        s[i] = s[j];
        s[j] = c;
    }
}

/* itoa:  convert n to characters in s */
void itoa(int n, char s[]) {
    int i, sign;

    if ((sign = n) < 0) /* record sign */
			n = -n; /* make n positive */
    i = 0;
    do { /* generate digits in reverse order */
        s[i++] = n % 10 + '0'; /* get next digit */
    }
    while ((n /= 10) > 0); /* delete it */
    if (sign < 0)
        s[i++] = '-';
    s[i] = '\0';
    reverse(s);
}

/**
 * Actions to be executed when a child has changed its state
 * @param sig
 * @return
 */
void sigchldHandler(int sig) {
    int status;
    int pid;
    int code;
    int signum;
    char message[MAX_MESSAGE + 1];


    while ((pid = waitpid(-1, &status, WUNTRACED | WCONTINUED | WNOHANG)) > 0) {
			signalReceived = analyze_status(status, &info);
			if (pid_fork == pid) {
				signalWaiting = 0;
			}
      if (WIFEXITED(status)) {
          code = WEXITSTATUS(status);
					strncpy(message, "Task: ", MAX_MESSAGE);
          itoa(pid,&message[strlen(message)]);
          strncat(message, " terminated with exit(", MAX_MESSAGE - strlen(message));
          itoa(code,&message[strlen(message)]);
          strncat(message, ")\n", MAX_MESSAGE - strlen(message));
          write(2, message, strlen(message));
      }
      if (WIFSIGNALED(status)) {
          signum = WTERMSIG(status);
					strncpy(message, "Task: ", MAX_MESSAGE);
        	itoa(pid,&message[strlen(message)]);
          strncat(message, " killed by signal ", MAX_MESSAGE - strlen(message));
          itoa(signum,&message[strlen(message)]);
          strncat(message, "\n", MAX_MESSAGE - strlen(message));
          write(2, message, strlen(message));
      }
      if (WIFSTOPPED(status)) {
          signum = WSTOPSIG(status);
          strncpy(message, "Task:  ", MAX_MESSAGE);
          itoa(pid,&message[strlen(message)]);
          strncat(message, " stopped by signal ", MAX_MESSAGE - strlen(message));
          itoa(signum,&message[strlen(message)]);
          strncat(message, "\n", MAX_MESSAGE - strlen(message));
          write(2, message, strlen(message));
      }
      if (WIFCONTINUED(status)) {

          strncpy(message, "parent: child ", MAX_MESSAGE);
          itoa(pid,&message[strlen(message)]);
          strncat(message, " continued\n", MAX_MESSAGE - strlen(message));
          write(2, message, strlen(message));
					signalWaiting = 1;
					return;
      }


			job* changedJob = get_item_bypid(taskList, pid);
			if (changedJob == NULL) {

			} else {
				if (changedJob->state == FOREGROUND) {
					signalWaiting = 0;
				}
				switch (signalReceived) {
					case 0: //Suspended
						changedJob->state=STOPPED;
						break;
					case 1: //Signaled
					case 2: //Exited
						delete_job(taskList, changedJob);
						break;
					default:
						break;
				}
    	}
		}
	}

// -----------------------------------------------------------------------
//                            MAIN
// -----------------------------------------------------------------------

int main(void) {
	char inputBuffer[MAX_LINE]; /* buffer to hold the command entered */
	//int background;             /* equals 1 if a command is followed by '&' */
	char *args[MAX_LINE/2];     /* command line (of 256) has max of 128 arguments */
	// probably useful variables:
	int status;             /* status returned by wait */
	enum status status_res; /* status processed by analyze_status() */
	//int info;				/* info processed by analyze_status() */


	int commandNum; //
	char currentPath[1000]; // Current path
	char* path = currentPath;
	int lastSignal; //Text to display the result of analyze_status
	char* user = getenv("USER"); //Get user's username
	char hostname[1024]; //Hostname
	hostname[1023] = '\0';

	gethostname(hostname,1023);//get user's hostname
	getcwd(path,999); //Gets the path
	ignore_terminal_signals(); //Ignore terminal calls to prevent the shell from closing
	signalWaiting = 0;
	signal(SIGCHLD,sigchldHandler);
	taskList = new_list("list of jobs");

	while (1) {  /* Program terminates normally inside get_command() after ^D is typed*/

		printf("%s%s@%s->%s ",KGRN,user,hostname,KNRM);
		fflush(stdout);
		get_command(inputBuffer, MAX_LINE, args, &background);  /* get next command */

		if(args[0]==NULL) continue;   // if empty command

		if(commandNum = checkIfInternal(args[0],internalCommands)) { //If the command is internal
			chooseCommand(commandNum,path,args, taskList, &signalWaiting);
			continue;
		}

		pid_fork = fork();

		if (pid_fork == -1) { //Check for error in creation of child
			fprintf(stderr, "Error: Child proccess not created\n");
			continue;
		}

		if (pid_fork == 0) {//Code executed on the child
			restore_terminal_signals(); //Restores terminal signals for the child process

			execvp(args[0],args);

			printf("Error 404: %s command not found\n",args[0]); // If execs fails
			exit(EXIT_FAILURE);


		} else { //Code executed by the parent

			new_process_group(pid_fork); //Gives a new group id to the child
			if (!background) {
				set_terminal(pid_fork); //If is a foreground process gives it terminal
				signalWaiting = 1;
	      while(signalWaiting) {
					pause();
				}
				switch (signalReceived) {
					case 0: //Suspended
						block_SIGCHLD();
						add_job(taskList, new_job(pid_fork, args[0], STOPPED));
						unblock_SIGCHLD();
						break;
					case 1: //Signaled
						break;
					case 2: //Exited
						break;
					default:
						break;
				}
				set_terminal(getpid());
				//printf("Foreground pid: %d, command: %s, %s, info: %d\n", pid_wait, args[0], status_strings[newSignal], info);

      }
			if (background) {
				block_SIGCHLD();
				add_job(taskList, new_job(pid_fork, args[0], BACKGROUND));
				unblock_SIGCHLD();
				printf("Background job running... pid: %d, command: %s\n", pid_fork, args[0]);
			}

		}
	} // end while
}
