#include "internal_commands.h"


void chooseCommand(int commandNum, char* currentPath, char* args[], job* list, int* signalWaiting) {
  int num;
  switch(commandNum) {
    case 1:
      if(args[1] == NULL) {
        chdir(getenv("HOME"));
      } else if(chdir(args[1]) != 0) {
        perror("Path not found");
        break;
      }
      cd(currentPath);

      break;
    case 2:
      pwd(currentPath);
      break;
    case 3:
      jobs(list);
      break;
    case 4:
    if(args[1] == NULL) {
      fg(list, -1, signalWaiting);
    } else {
      num = strtol(args[1],NULL,10);
      fg(list, num, signalWaiting);
    }
    break;
      break;
    case 5:
      if(args[1] == NULL) {
        bg(list, -1);
      } else {
        num = strtol(args[1],NULL,10);
        bg(list, num);
      }
      break;
  }
}


void cd(char* currentPath) {
  getcwd(currentPath,999);

}


void pwd(char* currentPath) {
  printf("%s\n", currentPath);
}

void jobs(job* list) {
  block_SIGCHLD();
  if (empty_list(list)) {
    printf("No jobs in the background or stopped.\n");
  } else {
    print_job_list(list);
  }
  unblock_SIGCHLD();
}

void fg(job* list, int position, int* signalWaiting) {
  block_SIGCHLD();
  job* jobToChange;

  if (empty_list(list)) {
    printf("No jobs in the background or stopped.\n");
    return;
  }
  if (position <= 0) {
    jobToChange = get_item_bypos(list, 1);
  } else if (position > list_size(list)) {
    printf("No item in position %d\nUse 'jobs' to check current job list\n", position);
    unblock_SIGCHLD();
    return;
  } else {
    jobToChange = get_item_bypos(list, position);
  }

  printf("Job pid: %d, command: %s is now running in the foreground\n", jobToChange->pgid, jobToChange->command);
  jobToChange->state = FOREGROUND;
  unblock_SIGCHLD();
  killpg(jobToChange->pgid,SIGCONT);

  set_terminal(jobToChange->pgid);
  int status, info;
  *signalWaiting = 1;
  while(*signalWaiting) {
    pause();
  }
  block_SIGCHLD();
  int newSignal = analyze_status(status, &info);
  switch (newSignal) {
    case 0: //Suspended
      jobToChange->state = STOPPED;
      break;
    case 1: //Signaled
      break;
    case 2: //Exited
      break;
    default:
      break;
  }
  set_terminal(getpid());
  //printf("Foreground pid: %d, command: %s, %s, info: %d\n", jobToChange->pgid, jobToChange->command, status_strings[newSignal], info);
  if (jobToChange->state == FOREGROUND) {
    delete_job(list, jobToChange);
  }
  unblock_SIGCHLD();
}

void bg(job* list, int position) {
  block_SIGCHLD();
  job* jobToChange;
  if (empty_list(list)) {
    printf("No jobs in the background or stopped.\n");
    return;
  }
  if (position <= 0) {
    jobToChange = get_item_bypos(list, 1);
  } else if (position > list_size(list)) {//Incorrent position exit safely
    printf("No item in position %d\nUse 'jobs' to check current job list\n", position);
    unblock_SIGCHLD();
    return;
  } else {
    jobToChange = get_item_bypos(list, position);
  }
  if (jobToChange->state != BACKGROUND) {
    jobToChange->state=BACKGROUND;
    killpg(jobToChange->pgid,SIGCONT);
    printf("Job pid: %d, command: %s is now running in the background\n", jobToChange->pgid, jobToChange->command);
  } else {
    printf("Job pid: %d, command: %s was already running in the background\n", jobToChange->pgid, jobToChange->command);
  }
  unblock_SIGCHLD();
}
