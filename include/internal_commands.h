#ifndef _INTERNAL_COMMANDS_H
#define _INTERNAL_COMMANDS_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "job_control.h"

static char* internalCommands = ",cd,pwd,jobs,fg,bg"; //List of internal commands

void chooseCommand(int commandNum, char* currentPath, char* args[], job* list, int* signalWaiting);

void cd(char* currentPath); //changes only internal path

void pwd(char* currentPath);

void jobs(job* list);

void fg(job* list, int position, int* signalWaiting);

void bg(job* list, int position);

#endif
